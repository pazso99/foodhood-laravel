## Database informations
- copy and replace .env.example

## Install dependencies
- npm install
- composer install

## Migrations, seeds
- php artisan migrate --seed

## Dev server
- php artisan serve