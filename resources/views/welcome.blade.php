@extends('layouts.app')

@section('title', 'Welcome!')
@section('content')
<div class="container">
    <h1 class="text-center">FoodHood</h1>
    <carousel :foods="{{ $foods }}"></carousel>
    <welcome></welcome>
</div>
@endsection