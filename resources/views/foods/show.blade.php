@extends('layouts.app')

@section('title', $food->name)
@section('content')
<div class="container">
    <food :added="{{ $added ? 'true' : 'false'}}" :food="{{ $food }}"></food>
</div>
@endsection
