@extends('layouts.app')

@section('title', 'Food list')
@section('content')
<div class="container">
    <h1 class="text-center">Food list</h1>
    <food-list :foods="{{ $foods }}"></food-list> 
</div>
@endsection
