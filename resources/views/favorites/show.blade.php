@extends('layouts.app')

@section('title', 'Favorites')
@section('content')
<div class="container">
    <h1 class="text-center">My favorites</h1>
    <food-list :foods="{{ $foods }}"></food-list> 
</div>
@endsection
