@extends('layouts.app')

@section('title', 'Contact')
@section('content')
<div class="container">
    <h1 class="text-center">Contact us</h1>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" aria-label="Close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
            </button>    
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <form method="post" action="/contact/send">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Enter Your Name</label>
            <input type="text" name="name" class="form-control" value="" />
        </div>

        <div class="form-group">
            <label>Enter Your Email</label>
            <input type="text" name="email" class="form-control" value="" />
        </div>

        <div class="form-group">
            <label>Enter Your Message</label>
            <textarea name="message" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <input type="submit" name="send" class="btn btn-info" value="Send" />
        </div>
    </form>
</div>
@endsection
