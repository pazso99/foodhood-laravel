@extends('layouts.app')

@section('title', 'Profile')
@section('content')
<div class="container">
    <h1 class="text-center">My profile</h1>
    <user :user="{{ $user }}"></user>
</div>
@endsection
