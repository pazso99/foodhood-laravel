<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FavoriteController;
use App\Http\Controllers\EmailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [FoodController::class, 'index']);
Route::get('/foods', [FoodController::class, 'index']);
Route::get('/foods/{id}', [FoodController::class, 'show']);

Route::get('/user/edit', [UserController::class, 'edit']);
Route::get('/user', [UserController::class, 'show']);
Route::patch('/user/{id}', [UserController::class, 'update']);

Route::get('/favorites', [FavoriteController::class, 'show']);
Route::post('/favorites/{id}', [FavoriteController::class, 'store']);
Route::post('/favorites/increment/{id}', [FavoriteController::class, 'increment']);
Route::post('/favorites/decrement/{id}', [FavoriteController::class, 'decrement']);

Route::get('/contact', [EmailController::class, 'index']);
Route::post('/contact/send', [EmailController::class, 'send']);


