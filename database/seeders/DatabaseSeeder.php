<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Food;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FoodTableSeeder::class);
        $this->call(TypeTableSeeder::class);
    }
}
