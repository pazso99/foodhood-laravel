<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Type;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::truncate();

        $types = [
            ['name' => 'burger'],
            ['name' => 'sandwich'],
            ['name' => 'shake'],
            ['name' => 'muffin'],
            ['name' => 'burrito'],
            ['name' => 'cookie'],
            ['name' => 'taco'],
            ['name' => 'hotdog'],
            ['name' => 'chicken'],
            ['name' => 'donut'],
            ['name' => 'drink'], 
            ['name' => 'pizza'],
            ['name' => 'pancake'],
            ['name' => 'bacon']
        ];
        
        Type::insert($types);
    }
}
