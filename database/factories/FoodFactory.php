<?php

namespace Database\Factories;

use App\Models\Food;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FoodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Food::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $types = [
            'burger',
            'sandwich',
            'shake',
            'muffin',
            'burrito',
            'cookie',
            'taco',
            'hotdog',
            'chicken',
            'donut',
            'drink', 
            'pizza',
            'pancake',
            'bacon'
        ];
        $type = $this->faker->randomElement($types);

        $index = array_search($type, $types) + 1;

        $img = $type . $this->faker->numberBetween(1, 2) .'.jpg';

        $name = strtok($this->faker->name, " ") . "'s " . $this->faker->colorName . " " . ucfirst($type);

        $num = $this->faker->numberBetween(0, 100);
        if ($num > 85) {
            $popularity = $this->faker->numberBetween(80, 100);
        } else if ($num > 40) {
            $popularity = $this->faker->numberBetween(10, 30);
        } else {
            $popularity = $num;
        }

        $price = ceil($this->faker->numberBetween(500, 8000) / 10) * 10;

        return [
            'name' => $name,
            'description' => $this->faker->text,
            'price' => $price,
            'image' => $img,
            'type_id' => $index,
            'popularity' => $popularity 
        ];
    }
}
