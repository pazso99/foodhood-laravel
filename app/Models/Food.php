<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'foods';

    // one food can belong to many order
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    // one food can belong to many user (favorite)
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    // one food has one type
    public function type() 
    {
        return $this->belongsTo(Type::class); 
    } 
}
