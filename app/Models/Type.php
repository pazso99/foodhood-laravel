<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;

    protected $guarded = [];

    // one type can belong to many foods
    public function food()
    {
        return $this->hasMany(Food::class); 
    } 
}
