<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $guarded = [];

    // one order belongs to one user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // one order can belong to many user
    public function foods() 
    {		
        return $this->belongsToMany(Food::class);
    }
}