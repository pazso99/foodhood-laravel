<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class EmailController extends Controller
{
    function index()
    {
        return view('emails.contact');
    }

    function send(Request $request)
    {
    $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'message' => 'required'
    ]);

    $data = array(
        'name' => $request->name,
        'email' => $request->email,
        'message' => $request->message
    );

    //Mail::to('somemail@mail.mail')->send(new SendMail($data));
    return back()->with('success', 'Thanks for contacting us!');
    }
}
