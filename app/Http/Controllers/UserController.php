<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show user
     */
    public function show()
    {
        if (Auth::user()) {
            $user = User::find(Auth::user()->id);
            if ($user) {
                return view('users.show', ['user' => $user]);
            }
        }
    }
    /* public function show($id)
    {
        if (!is_numeric($id)) {
            return abort('404');
        }
            
        $user = User::findOrFail($id);
        
        return view('users.show', ['user' => $user]);
    } */

    /**
     * Show edit user page
     */
    /* public function edit($id) {
        $user = User::findOrFail($id);
        return view('users.edit', ['user' => $user]);
    } */
    public function edit() {
        if (Auth::user()) {
            $user = User::find(Auth::user()->id);
            if ($user) {
                return view('users.edit', ['user' => $user]);
            }
        }
    }

    public function update(Request $request) {
        $user = User::find(Auth::user()->id);
        if ($user) {
            if (Auth::user()->email == $request['email']) {
                $validate = request()->validate([
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255'],
                    'address' => ['required', 'string', 'max:255']
                ]);
            } else {
                $validate = request()->validate([
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'address' => ['required', 'string', 'max:255']
                ]);
            }
           
            if ($validate) {
                $user->name = $request['name'];
                $user->address = $request['address'];
                $user->email = $request['email'];
                $user->save();
                return redirect("/user");
            }
        }
    }
    /**
     * Edit user name and adress
     */
    /* public function update($id) {
        $user = User::findOrFail($id);
        $this->authorize('update', $user);
        $data = request()->validate([
            'name' => ['string', 'max:255'],
            'address' => ['string', 'max:255'],
        ]);
        Auth::user()->update($data);
        return redirect("/user/{$user->id}");
    } */
}
