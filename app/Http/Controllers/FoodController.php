<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Food;

class FoodController extends Controller
{
    /**
     * Index page: show top 5 popular food
     * Foods page: show all food
     */
    public function index(Request $request)
    {
        if ($request->route()->uri == "/") {
            $foods = Food::orderBy('popularity', 'desc')->limit(5)->get();
            return view('welcome', compact('foods'));
        } else if ($request->route()->uri == "foods") {
            $foods = Food::all();
            return view('foods.index', compact('foods'));
        }
    }
 
    /**
     * Show food
     */
    public function show($id)
    {
        if (!is_numeric($id)) {
            return abort('404');
        }

        $food = Food::findOrFail($id);
        // check if user has this as favorite
        $added = (auth()->user()) ? auth()->user()->foods->contains($food->id) : false; 
        return view('foods.show', ['food' => $food, 'added' => $added]);
    }

}
