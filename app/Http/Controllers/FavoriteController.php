<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Food;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Toogles favorite food between user and food
     */
    public function store($id)
    {
        $food = Food::findOrFail($id);
        
        Auth::user()->foods()->toggle($food);
    }

    public function increment($id)
    {
        $food = Food::findOrFail($id);
        $food->increment("popularity");
    }

    public function decrement($id)
    {
        $food = Food::findOrFail($id);
        $food->decrement("popularity");
    }

    /**
     * Get user's favorite foods
     */
    public function show() {
        
        if (Auth::user()) {
            $user = User::find(Auth::user()->id);
            
            if ($user) {
                $foods = $user->foods;
                return view('favorites.show', ['foods' => $foods]);
            }
        }
    }
}
